## systemd metrics
## colors in share/check_mk/web/plugins/metrics/check_mk.py

metric_info["systemd_units_min"] = {
        "title" : _("Units"),
        "unit"  : "count",
        "color" : "44/a",
        }

metric_info["systemd_units_max"] = {
        "title" : _("Units"),
        "unit"  : "count",
        "color" : "44/b",
        }

metric_info["systemd_activestate_failed"] = {
        "title" : _("Failed"),
        "unit"  : "count",
        "color" : "14/a",
        }

metric_info["systemd_activestate_active"] = {
        "title" : _("Active"),
        "unit"  : "count",
        "color" : "34/a",
        }

metric_info["systemd_activestate_inactive"] = {
        "title" : _("Inactive"),
        "unit"  : "count",
        "color" : "51/a",
        }

metric_info["systemd_activestate_reloading"] = {
        "title" : _("Reloading"),
        "unit"  : "count",
        "color" : "24/a",
        }

metric_info["systemd_activestate_activating"] = {
        "title" : _("Activating"),
        "unit"  : "count",
        "color" : "31/a",
        }

metric_info["systemd_activestate_deactivating"] = {
        "title" : _("Deactivating"),
        "unit"  : "count",
        "color" : "36/a",
        }

metric_info["systemd_loadstate_loaded"] = {
        "title" : _("Loaded"),
        "unit"  : "count",
        "color" : "34/a",
        }

metric_info["systemd_loadstate_error"] = {
        "title" : _("Error"),
        "unit"  : "count",
        "color" : "14/a",
        }

metric_info["systemd_loadstate_masked"] = {
        "title" : _("Masked"),
        "unit"  : "count",
        "color" : "44/a",
        }

metric_info["systemd_loadstate_not-found"] = {
        "title" : _("Not Found"),
        "unit"  : "count",
        "color" : "16/a",
        }

## systemd graphs

graph_info.append({
    "title"    : _("Systemd Units Activestate"),
    "metrics"  : [ 
        ( "systemd_activestate_active", "area" ),
        ( "systemd_activestate_inactive", "stack" ),
        ( "systemd_activestate_error", "stack" ),
        ( "systemd_activestate_reloading", "stack" ),
        ( "systemd_activestate_activating", "stack" ),
        ( "systemd_activestate_deactivating", "stack" ),
        ],
    "scalars"  : [
        ("systemd_units_min:warn#00FF00", "Units min warning"),
        ("systemd_units_min:crit#FF0080", "Units min critical"),
        ("systemd_units_max:warn#00FFAA", "Units max warning"),
        ("systemd_units_max:crit#FF00FF", "Units max critical"),
        ("systemd_activestate_failed:warn", "Failed warning"),
        ("systemd_activestate_failed:crit", "Failed critical"),
        ],
    "optional_metrics" : ["systemd_units_min", "systemd_units_max"]
    })


graph_info.append({
    "title"    : _("Systemd Units Loadstate"),
    "metrics"  : [ 
        ( "systemd_loadstate_loaded", "area" ),
        ( "systemd_loadstate_error", "stack" ),
        ( "systemd_loadstate_masked", "stack" ),
        ( "systemd_loadstate_not-found", "stack" ),
        ]
    })

