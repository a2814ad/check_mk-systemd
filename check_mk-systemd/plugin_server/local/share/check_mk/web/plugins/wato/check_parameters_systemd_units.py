#!/usr/bin/python
# -*- encoding: utf-8; py-indent-offset: 4 -*-

group = "checkparams"

subgroup_applications = _("Applications, Processes & Services")
subgroup_inventory =    _("Discovery - automatic service detection")


register_check_parameters(
    subgroup_applications,
    "systemd_units",
    _("Systemd services and units"),
    Dictionary(
        elements = [
        (   "levels",
            Tuple(
                title = _("Levels for failed units/services"),
                elements = [
                Integer( title = _("Warning if failed count is above"), default_value = 1 ),
                Integer( title = _("Critical if failed count is above"), default_value = 2 ),
                ],
            ),
        ),
        (
            "mincount",
            Tuple(
                title = _("Minimum Count of units/services"),
                elements = [
                Integer( title = _("Warning if number of units is below")),
                Integer( title = _("Critical if number of units is below")),
                ],
            ),
        ),
        (
            "maxcount",
            Tuple(
                title = _("Maximum Count of units/services"),
                elements = [
                Integer( title = _("Warning if number of units is above")),
                Integer( title = _("Critical if number of units is above")),
                ],
            ),
        ),
        (   'reqstate', ListChoice(
            choices = [
                ('active',     _('Active')),
                ('reloading',   _('Reloading')),
                ('inactive', _('Inactive')),
                ('failed', _('Failed')),
                ('activating', _('Activating')),
                ('deactivating', _('Deactivating')),
            ],
            title = _("Require active state"),
        )),
        (   'reqsubstate', ListChoice(
            choices = [
                ('dead',     _('Dead')),
                ('running',   _('Running')),
            ],
            title = _("Require active substate"),
        )),
        ],
        # optional_keys = ["levels", "expectedStrings"]
    ),
    TextAscii(
    title = _("Unit name"),
    help = _("Name of the systemd unit."),
    allow_empty = False
    ),
    "dict",
)

register_check_parameters(
    subgroup_applications,
    "systemd_units_summary",
    _("Systemd Unit Summary"),
    Dictionary(
        title = _("Systemd Units Summary Parameters"),
        elements = [
        (   "levels",
            Tuple(
                title = _("Levels for failed units/services"),
                elements = [
                Integer( title = _("Warning if failed count is above"), default_value = 1 ),
                Integer( title = _("Critical if failed count is above"), default_value = 2 ),
                ],
            ),
        ),
        (
            "mincount",
            Tuple(
                title = _("Minimum Count of units/services"),
                elements = [
                Integer( title = _("Warning if number of units is below")),
                Integer( title = _("Critical if number of units is below")),
                ],
            ),
        ),
        (
            "maxcount",
            Tuple(
                title = _("Maximum Count of units/services"),
                elements = [
                Integer( title = _("Warning if number of units is above")),
                Integer( title = _("Critical if number of units is above")),
                ],
            ),
        ),
        ('loadstate', ListChoice(
                choices = [
                    ('loaded', _('Loaded')),
                    ('error', _('Error')),
                    ('masked', _('Masked')),
                    ('not-found', _('Not found')),
                ],
                title = _("Filter load state"),
                default_value = ['loaded']
        )),
        ('activestate', ListChoice(
            choices = [
                ('active',     _('Active')),
                ('reloading',   _('Reloading')),
                ('inactive', _('Inactive')),
                ('failed', _('Failed')),
                ('activating', _('Activating')),
                ('deactivating', _('Deactivating')),
            ],
            title = _("Filter active state"),
        )),
        ],
        # optional_keys = ["levels", "expectedStrings"]
    ),
    None,
    "dict"
)


register_rule(group + '/' + subgroup_inventory,
    varname   = "inventory_systemd_unit_rules",
    title     = _("Systemd Unit and Service Discovery"),
    valuespec = Dictionary(
        elements = [
            ('units', ListOfStrings(
                title = _("Units/Services (Regular Expressions)"),
                help  = _('String matching the the name or description of the systemd unit.'
                          'If the string starts with ~, then it will be interpreted as regular expression, '
                          'matching the beginning of the name or description. '
                          'If no name is given then this rule will match all units. The '
                          'match is done on the <i>beginning</i> of the unit name. It '
                          'is done <i>case sensitive</i>. You can do a case insensitive match '
                          'by prefixing the regular expression with <tt>(?i)</tt>. Example: '
                          '<tt>(?i).*mysql</tt> matches all services which contain <tt>MYSQL</tt> '
                          'or <tt>MySQL</tt> or <tt>mysql</tt> or...'),

                orientation = "horizontal",
            )),
            ('loadstate', ListChoice(
                choices = [
                    ('loaded', _('Loaded')),
                    ('error', _('Error')),
                    ('masked', _('Masked')),
                    ('not-found', _('Not found')),
                ],
                title = _("Create check if unit is in load state"),
            )),
            ('activestate', ListChoice(
                choices = [
                    ('active',     _('Active')),
                    ('reloading',   _('Reloading')),
                    ('inactive', _('Inactive')),
                    ('failed', _('Failed')),
                    ('activating', _('Activating')),
                    ('deactivating', _('Deactivating')),
                ],
                title = _("Create check if unit is in active state"),
            )),
        ],
        help = _('This rule can be used to configure the inventory of the systemd units check. '
                 'You can configure specific systemd units to be monitored by the systemd check by '
                 'selecting them by name, current state during the inventory, or start mode.'),
    ),
    match = 'all',
)

