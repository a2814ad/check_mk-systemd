#!/usr/bin/env python

try:
    import os
    import sys
    import platform
    from gi.repository import Gio, GLib

except ImportError, detail:
    # silent exit
    sys.exit(0)
 


 
class SystemdDbus:
    def __init__(self):
        try:
            self.bus = Gio.bus_get_sync(Gio.BusType.SYSTEM, None)
            self.proxy = Gio.DBusProxy.new_sync( self.bus, Gio.DBusProxyFlags.NONE, None,
                              'org.freedesktop.systemd1', '/org/freedesktop/systemd1', 'org.freedesktop.systemd1.Manager', None)
        except:
            sys.exit(0)
 
    # https://www.freedesktop.org/wiki/Software/systemd/dbus/
    def get_units(self):
        output = GLib.Variant.unpack(self.proxy.call_sync('org.freedesktop.systemd1.Manager.ListUnits',None,Gio.DBusCallFlags.NONE,1000,None))
        return output
    
    def get_jobs(self):
        output = GLib.Variant.unpack(self.proxy.call_sync('org.freedesktop.systemd1.Manager.ListJobs',None,Gio.DBusCallFlags.NONE,1000,None))
        return output
    
    def get_default_target(self):
        output = GLib.Variant.unpack(self.proxy.call_sync('org.freedesktop.systemd1.Manager.GetDefaultTarget',None,Gio.DBusCallFlags.NONE,1000,None))
        return output

def print_units(s):
    print "<<<systemd_units:sep(124)>>>"
    services = s.get_units()
    for line in services[0]:
        # if line[2] != "loaded" or line[3] == "inactive":
        #   continue
        print('|'.join(map(str,line))) # or only print?
 
def print_jobs(s):
    print "<<<systemd_jobs>>>"
    jobs = s.get_jobs()
    print jobs[0]
   
def print_defaulttarget(s):
    print "<<<systemd_target>>>"
    target=s.get_default_target()
    print(target[0])
    
def main():
    # check if we have linux
    if platform.system() != "Linux":
        sys.exit(0)
 
    # check if it is a systemd based system
    if not os.path.isdir("/usr/lib/systemd"):
        sys.exit(0)
    s = SystemdDbus()
    print_units(s)
    # unused at the moment
    # print_jobs(s)
    print_defaulttarget(s)

    sys.exit(0)
 
if __name__ == "__main__":
    main()
    



