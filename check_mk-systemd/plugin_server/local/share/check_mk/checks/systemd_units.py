#!/usr/bin/python
# -*- encoding: utf-8; py-indent-offset: 4 -*-

# Systemd units

# default: number of unit warning/critical
factory_settings["systemd_units_default_values"] = {
    "levels" : (1,2)
}

inventory_systemd_unit_rules = []

def inventory_systemd_units(info):
    
    
    def add_matching_services(name, description, loadstate, activestate, rule):
        
        # filter for activestate and loadstate
        if rule.has_key('loadstate') and rule["loadstate"] and loadstate not in rule["loadstate"]:
            return []
        if rule.has_key('activestate') and rule["acitvestate"] and loadstate not in rule["activestate"]:
            return []

        # match name or description
        if rule.has_key('units') and rule["units"]:
            for unitname in rule["units"]: # FEHLEER: Hier wird beim ersten nicht-natch per return abgebrochen!
                if unitname.startswith("~"):
                    r = regex(unitname[1:])
                    if r.match(name) or r.match(description):
                        return [(name, {})]
                elif unitname == name or unitname == description:
                    return [(name, {})]
        else: # no filter for unit name
            return [(name, {})]
        return [] # filter for unit name, but no match
    
    
    rules = []
    for rule in inventory_systemd_unit_rules:
        # 1. Get all rules matching the current host
        #print rule
        # ({'units': ['apache', 'mysql', 'test'], 'acitvestate': ['active', 'failed'], 'loadstate': ['loaded']}, [], ['@all']) 
        taglist, hostlist = rule[1:3]
        if not hosttags_match_taglist(tags_of_host(host_name()), taglist) or not in_extraconf_hostlist(hostlist, host_name()):
            continue
        rules.append(rule[0])

    inventory = []
    for line in info:
        name = line[1]
        description = line[2]
        loadstate = line[3]
        activestate = line [4]
        substate = line [5]

        for rule in rules:
            inventory += add_matching_services(name, description, loadstate, activestate, rule)

    return inventory


def check_systemd_units(item, params, info):
    
    def state_filter(state,filter):
        if len(filter)==0:
            return True
        if state in filter:
            return True
        return False

    iteminfo=0
    for line in info:
        if item == line[1]:
            iteminfo = line
            break

    outstring="%s: Status: %s:%s" % (item, iteminfo[4], iteminfo[5])

    gstatusok = True
    if "reqstate" in params:
        if not state_filter(iteminfo[4], params["reqstate"]):
            gstatusok = False
            outstring += ", Required state: " + ",".join(params["reqstate"])

    if "reqsubstate" in params:
        if not state_filter(iteminfo[5], params["reqsubstate"]):
            gstatusok = False
            outstring += ", Required substate: " + ",".join(params["reqsubstate"])

    if gstatusok:
        return 0, outstring
    else:
        return 2, outstring
        
    
# check declaration
check_info["systemd_units"] = {
    "service_description"     : "Systemd Unit %s",
    "check_function"          : check_systemd_units,
    "inventory_function"      : inventory_systemd_units,
    'has_perfdata':              True,
    "node_info":                True,
    "group"                   : "systemd_units",
    "default_levels_variable" : "systemd_units_default_values",
}


#
# Systemd Units Summary
# 


factory_settings["systemd_units_summary_default_values"] = {
    "levels" : (1,2)
}

def inventory_systemd_units_summary(info):
    if info[0]:
        return [ (None, "systemd_units_summary_default_values") ]

def check_systemd_units_summary(item, params, info):

    def state_filter(state, filter):
        if len(filter) == 0:
            return True
        if state in filter:
            return True
        return False

    warnfailed, critfailed = params["levels"]
    checkmin = False
    checkmax = False
    minwarn = ""
    maxwarn = ""
    mincrit = ""
    maxcrit = ""
    if "mincount" in params:
        minwarn, mincrit = params["mincount"]
        checkmin = True
    if "maxcount" in params:
        maxwarn, maxcrit = params["maxcount"]
        checkmax = True
    loadstate_filter = []
    activestate_filter = []
    if "loadstate" in params:
        loadstate_filter = params["loadstate"]
    if "activestate" in params:
        activestate_filter = params["activestate"]

    numfailed = 0
    numunits = 0
    failedunits = []
    for line in info:
        if item == None or item == line[1]:
            loadstate = line[3]
            activestate = line[4]
            if state_filter(loadstate, loadstate_filter) == False or state_filter(activestate,
                                                                                  activestate_filter) == False:
                continue
            numunits += 1
            if line[4] == "failed":  # and line[5]=="failed":
                failedunits.append(line[1])
                numfailed += 1
    outstring = "%s: %d, Failed: %d" % ("Summary", numunits, numfailed)
    perfdata = [("failed", numfailed, warnfailed, critfailed, 0)]
    if checkmin:
        perfdata.append(("units", numunits, minwarn, mincrit, 0))
    elif checkmax:
        perfdata.append(("units", numunits, maxwarn, maxcrit, 0))

    if numfailed > 0:
        outstring += " (%s)" % (','.join(failedunits))

    if numfailed > critfailed or (checkmin and numunits < mincrit) or (checkmax and numunits > maxcrit):
        return 2, outstring, perfdata
    if numfailed > warnfailed or (checkmin and numunits < minwarn) or (checkmax and numunits > maxwarn):
        return 1, outstring, perfdata
    return 0, outstring, perfdata
        
    
# check declaration
check_info["systemd_units.summary"] = {
    "service_description"     : "Systemd Unit Summary",
    "check_function"          : check_systemd_units_summary,
    "inventory_function"      : inventory_systemd_units_summary,
    'has_perfdata':              True,
    "node_info":                True,
    "group"                   : "systemd_units_summary",
    "default_levels_variable" : "systemd_units_summary_default_values",
}


